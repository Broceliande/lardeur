package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LootPage extends HomePage {
	
	@FindBy(xpath = "//h1")
	private WebElement dropName;
	
	@FindBy(xpath = "//div[starts-with(@id,'tt')]")
	private WebElement lootInfo;

	public LootPage(WebDriver driver) {
		super(driver);
	}

	public WebElement getDropName() {
		return dropName;
	}

	public WebElement getLootInfo() {
		return lootInfo;
	}
}
