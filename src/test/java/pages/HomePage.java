package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends PageGenerator{
	
	@FindBy(xpath="//input[@name='q']")
	private WebElement inputSearch;
	
	@FindBy(xpath = "//form[contains(@action, 'search')]/following-sibling::a")
	private WebElement buttonSearch;
	
	protected WebDriverWait wait;
	
	public WebElement getButtonSearch() {
		return buttonSearch;
	}


	public HomePage (WebDriver driver) {
		super(driver);
	}


	public WebElement getInputSearch() {
		return inputSearch;
	}


	public <T> void click (T element) {
		if(element.getClass().getName().contains("By")) {
			driver.findElement((By) element).click();
		} else {
			((WebElement) element).click();
		}
	}

	public <T> void sendText (T element, String text) {
		if(element.getClass().getName().contains("By")) {
			driver.findElement((By) element).sendKeys(text);
		} else {
			((WebElement) element).sendKeys(text);
		}
	}
	
	public void wait(By element, int time) {
		wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
//	public <T, P> P click (T element, P page) {
//		if(element.getClass().getName().contains("By")) {
//			driver.findElement((By) element).click();
//		} else {
//			((WebElement) element).click();
//		}
//	}
}
