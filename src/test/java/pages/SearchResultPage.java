package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends HomePage{
	
	@FindBy(xpath="//div[@id='search-tabs']//a[@href='#npcs']")
	private WebElement NPCtab;
	

	public SearchResultPage(WebDriver driver) {
		super(driver);
	}
	
	
//	public void getNPC(String pnj) {
//		List<WebElement> NPClist;
//		NPClist = driver.findElements(By.xpath("//div[@id='tab-npcs']//tbody[@class='clickable']/tr/td[2]/a"));
//		
//	}
	
	public WebElement getNPCtab() {
		return NPCtab;
	}


	public WebElement getBoss(String boss) {
		return driver.findElement(By.xpath("//div[@id='tab-npcs']//tbody[@class='clickable']/tr/td[contains(@class,'boss')]/a[contains(.,'" + boss + "')]"));
	}
}
