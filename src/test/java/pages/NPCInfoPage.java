package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NPCInfoPage extends HomePage {
	
	public NPCInfoPage(WebDriver driver) {
		super(driver);
	}
	
	public List<WebElement> getLoots() {
		return driver.findElements(By.xpath("//div[@id='tab-drops']//tbody/tr/td[3]//a"));
	}
	
	public WebElement getLootByName(String name) {
		wait(By.xpath("//div[@id='tab-drops']//tbody/tr/td[3]//a[.='" + name + "']/../.."), 10);
		return driver.findElement(By.xpath("//div[@id='tab-drops']//tbody/tr/td[3]//a[.='" + name + "']/../.."));
	}
	
//	public String getLootRarityByName(String name) {
//		return driver.findElement(By.xpath("//div[@id='tab-drops']//tbody/tr/td[3]//a[.='" + name + "']")).getCssValue("color");
//		
//	}
	
	public String getLootRarity() {
		return driver.findElement(By.xpath("//div[@id='tab-drops']//tbody/tr/td[3]//a")).getCssValue("color");
		
	}
	
}
