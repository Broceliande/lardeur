package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class PageGenerator {

	protected WebDriver driver;
	 
    public PageGenerator(WebDriver driver){
        this.driver = driver;
    }
 
    public  <PAGE extends HomePage> PAGE getInstance (Class<PAGE> pageClass) throws Exception {
        try {
            return PageFactory.initElements(driver,  pageClass);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    public void scroll(int ab, int ord ) {
    	JavascriptExecutor jse = (JavascriptExecutor) driver;
    	// Scroll vers le bas de 1000 pixels
    	jse.executeScript("window.scrollBy(" + ab + "," + ord + ")");
	}
}
