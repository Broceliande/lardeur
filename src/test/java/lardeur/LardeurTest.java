package lardeur;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.HomePage;
import pages.LootPage;
import pages.NPCInfoPage;
import pages.PageGenerator;
import pages.SearchResultPage;

public class LardeurTest {
	
	private WebDriver driver;
	
	private String url = "https://fr.wowhead.com";
	private String search = "Lardeur";
	private String lootName1 = "Chahuteurs de cadavre";
	private String lootName2 = "Chausses de Lardeur";
	private String browser = System.getProperty("browser");
	
	@Before
	public void init() {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setJavascriptEnabled(false);
		
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			driver = new ChromeDriver(capabilities);
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions(capabilities);
			driver = new FirefoxDriver(options);
		}
		
		//options.setPageLoadStrategy(PageLoadStrategy.NONE);
		//options.addArguments("--disable-gpu");
		
		//System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
		//driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebDriverWait wdw = new WebDriverWait(driver, 10);
		//driver.manage().timeouts().pageLoadTimeout(40000, TimeUnit.MILLISECONDS);
		driver.get(url);
			
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		wdw.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("/html/body/div[9]/div[1]/div/div/div[3]/button")))).click();
		
		//driver.findElement(By.tagName("body")).sendKeys("Keys.ESCAPE");
		
		
	}
	
	@After
	public void teardown() {
		driver.quit();
	}
	
	@Ignore
	public void testi() {
		
	}
	
	@Test
	public void test() throws Exception {
		PageGenerator pgGenerator = PageFactory.initElements(driver, PageGenerator.class);
		HomePage homePage = pgGenerator.getInstance(HomePage.class);
		homePage.sendText(homePage.getInputSearch(), search);
		homePage.click(homePage.getButtonSearch());
		
		SearchResultPage searchResultPage = homePage.getInstance(SearchResultPage.class);
		searchResultPage.scroll(0, 700);
		searchResultPage.click(searchResultPage.getNPCtab());
		//Thread.sleep(2000);
		searchResultPage.click(searchResultPage.getBoss(search));
		NPCInfoPage npcInfoPage = searchResultPage.getInstance(NPCInfoPage.class);
		npcInfoPage.scroll(0, 1300);
		assertEquals("nombre de butins", 5, npcInfoPage.getLoots().size());
		
		if(driver instanceof ChromeDriver) {
			assertEquals("Loots are rare", "rgba(0, 112, 221, 1)", npcInfoPage.getLootRarity());
		} else if(driver instanceof FirefoxDriver) {
			assertEquals("rarity", "rgb(0, 112, 221)", npcInfoPage.getLootRarity());
		}
			
		npcInfoPage.click(npcInfoPage.getLootByName(lootName1));
		npcInfoPage.scroll(0, 300);
		LootPage lootPage = npcInfoPage.getInstance(LootPage.class);
		System.out.println(lootPage.getLootInfo().getText());
		assertEquals("nom du butin", lootName1, lootPage.getDropName().getText());
		assertTrue("STATS1", lootPage.getLootInfo().getText().contains("+5 Intelligence"));
		assertTrue("STATS2", lootPage.getLootInfo().getText().contains("+8 Endurance"));
		assertTrue("STATS3", lootPage.getLootInfo().getText().contains("+3 Versatilité"));
		
		driver.navigate().back();
		
		npcInfoPage.click(npcInfoPage.getLootByName(lootName2));
		npcInfoPage.scroll(0, 300);
		System.out.println(lootPage.getLootInfo().getText());
		assertEquals("nom du butin", lootName2, lootPage.getDropName().getText());
		assertTrue("STATS", lootPage.getLootInfo().getText().contains("+7 [Agilité or Intelligence]"));
		assertTrue("STATS2", lootPage.getLootInfo().getText().contains("+10 Endurance"));
		assertTrue("STATS3", lootPage.getLootInfo().getText().contains("Augmente votre score de coup critique de +6"));
		assertTrue("STATS4", lootPage.getLootInfo().getText().contains("+3 Versatilité"));
	}

}
